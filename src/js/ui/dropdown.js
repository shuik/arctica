const   dropdownsToggle = document.querySelectorAll('[data-dropdown-toggle]'),
        dropdowns = document.querySelectorAll('[data-dropdown]');

function isHidden(el) {
    const style = window.getComputedStyle(el);
    return (style.display === 'none');
}

let currentToggle;

let statusClick = false;

function showDropdown(item, dropdown, pos){
    if(pos){
        const top = item.offsetTop;
        const left = item.offsetLeft;

        dropdown.style.left = left - 25 + "px";
        dropdown.style.top = top + 30 + "px";
    }
    
    if(isHidden(dropdown)){
        if(currentToggle != item){
            dropdown.classList.add('active');
        }
    }
    
    if(currentToggle != item){
        currentToggle = item;
    }else{
        currentToggle = null;
    }

    statusClick = true; 
}

document.addEventListener('click', function(e) {
    statusClick = false;

    dropdowns.forEach(item => {
        if(!isHidden(item)){
            item.classList.remove('active');
        }
    });

    dropdownsToggle.forEach(item => {
        const data = item.getAttribute('data-dropdown-toggle'),
                dropdown = document.querySelector(`[data-dropdown=${data}]`),
                pos = item.getAttribute('data-dropdown-pos');

        if(e.target === item){
            showDropdown(item, dropdown, pos);
        }else{
            for(var x = 0; x < item.children.length; x++){
                if(e.target == item.children[x]){
                    showDropdown(item, dropdown, pos);
                }
            }
        }
    });

    if(!statusClick){
        currentToggle = null;
    }
});