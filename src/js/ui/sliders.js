const statisticSlider = document.querySelector('.b-statistic-slider__container');

if(statisticSlider){
    const slider = tns({
        container: statisticSlider,
        items: 4,
        gutter: 23,
        nav: false,
        rewind: true,
        controlsText: ['', '<i class="las la-arrow-right"></i>'],
        responsive: {
            0: {
                items: 2,
            },
            1350: {
                items: 3,
            },
            1650: {
                items: 4,
            }
        }
    });
}