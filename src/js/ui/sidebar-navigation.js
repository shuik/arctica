const   hum = document.querySelector('.hum'),
        sidebar = document.querySelector('.sidebar'),
        logo = document.querySelector('.sidebar .logo'),
        nav = document.querySelector('.sidebar .nav'),
        contentWrap = document.querySelector('.content__wrap'),
        contentSidebar = document.querySelector('.content__left'),
        contentBody = document.querySelector('.content__body');


if(hum){
    let width = contentSidebar.offsetWidth;

    contentBody.style.maxWidth = `calc(100% - 23px - ${width}px)`;

    hum.addEventListener('click', e => {
        e.preventDefault();

        sidebar.classList.toggle('active');
        logo.classList.toggle('active');
        nav.classList.toggle('active');    
        contentWrap.classList.toggle('active');

        if(contentSidebar && contentBody){
            width = contentSidebar.offsetWidth;

            contentBody.style.maxWidth = `calc(100% - 23px - ${width}px)`;
        }
    });
}