const   modalTriggers = document.querySelectorAll('[data-modal]'),
        modalCloseTrigger = document.querySelectorAll('[data-modal-close]'),
        overlay = document.querySelector('.modal__overlay'),
        modals = document.querySelectorAll('.modal');

function hideModal () {
    modals.forEach(item => {
        item.classList.remove('is--visible');
    });
    overlay.classList.remove('is--visible');

    document.body.classList.remove('fixed');
}

function showModal(data) {
    overlay.classList.add('is--visible');

    const modal = document.querySelector(`[data-popup-modal="${data}"]`);

    modal.classList.add('is--visible');

    document.body.classList.add('fixed');
}

modals.forEach(modal => {
    const close = document.createElement('div');
    close.classList.add('modal__close');
    close.innerHTML = '<i class="las la-times"></i>';
    modal.querySelector('.modal__container').append(close);
    close.addEventListener('click', hideModal);
});

modalTriggers.forEach(item => {
    item.addEventListener('click', e => {
        e.preventDefault();

        const { modal } = item.dataset;

        showModal(modal);
    });
});

modalCloseTrigger.forEach(item => {
    item.addEventListener('click', e => {
        e.preventDefault();

        hideModal();
    });
});

window.addEventListener('click', function(e){   
    const modal = document.querySelector('.modal.is--visible');

    if (e.target && e.target == modal){
        hideModal();
    }
});