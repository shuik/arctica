const docsToggles = document.querySelectorAll('.js-docs-list-toggle');

if(docsToggles.length > 0){
    docsToggles.forEach(item => {
        item.addEventListener('click', () => {
            item.classList.toggle('active');
            slideToggle(item.parentElement.parentElement.parentElement.parentElement.querySelector('.docs-lists__body'));
        });
    });
}