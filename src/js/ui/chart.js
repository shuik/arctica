const lineChartOptions = {
    legend: {
        display: false
    },
    responsive: false,
    elements: {
        line:{
            tension: '0.000001'
        }
    },
    scales: {
        yAxes: [{
            display: false,
            ticks: {
                suggestedMax: 11, //largest num of data + 1
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true,
                padding: 0
            },
            gridLines: {
                drawTicks: false,
                display: false
            }
        }],
        xAxes: [{
            gridLines: {
                zeroLineColor: "transparent"
            },
            ticks: {
                padding: 15,
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
            }
        }],
    },
    tooltips: {
        callbacks: {
            label: function(tooltipItem) {
                const num = Number(tooltipItem.yLabel);
                let text = "проектов";
                if(num == 1){
                    text = "проект";
                }else if(num % 10 > 1 && num % 10 < 5){
                    text = "проекта";
                }
                return num + " " + text;
            }
        },
        backgroundColor: "#fff",
        titleFontFamily: 'Lato',
        titleMarginBottom: 10,
        xPadding: 20,
        yPadding: 15,
        bodySpacing: 10,
        titleFontSize: 15,
        titleFontStyle: 'normal',
        titleFontColor: '#8181A5',
        bodyFontFamily: 'Lato',
        bodyFontColor: '#1C1D21',
        bodyFontSize: 13,
        bodyFontStyle: '700',
        shadowOffsetX: 0,
        shadowOffsetY: 8,
        shadowBlur: 8,
        shadowColor: 'rgba(152, 169, 188, 0.267182)',
    }
};
const lineDoubleChartOption = {
    legend: {
        display: false
    },
    responsive: false,
    elements: {
        line:{
            tension: '0.000001'
        }
    },
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true,
                padding: 20
            },
            gridLines: {
                drawTicks: false,
                display: false
            }
        }],
        xAxes: [{
            gridLines: {
                zeroLineColor: "transparent"
            },
            ticks: {
                padding: 15,
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
            }
        }],
    },
    tooltips: {
        callbacks: {
            label: function(tooltipItem) {
                const num = Number(tooltipItem.yLabel);
                let text = "проектов";
                if(num == 1){
                    text = "проект";
                }else if(num % 10 > 1 && num % 10 < 5){
                    text = "проекта";
                }
                return num ;
            }
        },
        backgroundColor: "#fff",
        titleFontFamily: 'Lato',
        titleMarginBottom: 10,
        xPadding: 20,
        yPadding: 15,
        bodySpacing: 10,
        titleFontSize: 15,
        titleFontStyle: 'normal',
        titleFontColor: '#8181A5',
        bodyFontFamily: 'Lato',
        bodyFontColor: '#1C1D21',
        bodyFontSize: 13,
        bodyFontStyle: '700',
        shadowOffsetX: 0,
        shadowOffsetY: 8,
        shadowBlur: 8,
        shadowColor: 'rgba(152, 169, 188, 0.267182)',
    }
};
const barChartOption = {
    legend: {
        display: false
    },
    responsive: false,
    elements: {
        line:{
            tension: '0.000001'
        }
    },
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true,
                padding: 20
            },
            gridLines: {
                drawTicks: false,
                display: false
            }
        }],
        xAxes: [{
            gridLines: {
                zeroLineColor: "transparent"
            },
            ticks: {
                padding: 15,
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
            }
        }],
    },
    tooltips: {
        callbacks: {
            title: function() {},
            beforeTitle: function(tooltipItem, data) {
                const index = tooltipItem[0].datasetIndex;
                return data.datasets[index].label;
            },
            label: function(tooltipItem) {
                const num = Number(tooltipItem.yLabel);
                let text = "лет";
                return num + " " + text;
            }
        },
        backgroundColor: "#fff",
        titleFontFamily: 'Lato',
        titleMarginBottom: 10,
        xPadding: 20,
        yPadding: 15,
        bodySpacing: 10,
        titleFontSize: 15,
        titleFontStyle: 'normal',
        titleFontColor: '#8181A5',
        bodyFontFamily: 'Lato',
        bodyFontColor: '#1C1D21',
        bodyFontSize: 15,
        bodyFontStyle: '700',
        shadowOffsetX: 0,
        shadowOffsetY: 8,
        shadowBlur: 8,
        shadowColor: 'rgba(152, 169, 188, 0.267182)',
        displayColors: false,
    }
};
const barSmallChartOption = {
    legend: {
        display: false
    },
    responsive: false,
    elements: {
        line:{
            tension: '0.000001'
        }
    },
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true,
                padding: 20,
                display: false
            },
            gridLines: {
                drawTicks: false,
                display: false
            }
        }],
        xAxes: [{
            gridLines: {
                zeroLineColor: "transparent"
            },
            ticks: {
                padding: 0,
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
            }
        }],
    },
    tooltips: {
        callbacks: {
            title: function() {},
            beforeTitle: function(tooltipItem, data) {
                const index = tooltipItem[0].datasetIndex;
                return data.datasets[index].label;
            },
            label: function(tooltipItem) {
                const num = Number(tooltipItem.yLabel);
                let text = "лет";
                return num + " " + text;
            }
        },
        backgroundColor: "#fff",
        titleFontFamily: 'Lato',
        titleMarginBottom: 10,
        xPadding: 20,
        yPadding: 15,
        bodySpacing: 10,
        titleFontSize: 15,
        titleFontStyle: 'normal',
        titleFontColor: '#8181A5',
        bodyFontFamily: 'Lato',
        bodyFontColor: '#1C1D21',
        bodyFontSize: 15,
        bodyFontStyle: '700',
        shadowOffsetX: 0,
        shadowOffsetY: 8,
        shadowBlur: 8,
        shadowColor: 'rgba(152, 169, 188, 0.267182)',
        displayColors: false,
    }
};
const barHorzChartOption = {
    legend: {
        display: false
    },
    responsive: false,
    elements: {
        line:{
            tension: '0.000001'
        }
    },
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true,
                padding: 20,
            },
            gridLines: {
                drawTicks: false,
                display: false
            }
        }],
        xAxes: [{
            gridLines: {
                zeroLineColor: "transparent"
            },
            ticks: {
                padding: 0,
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true
            }
        }],
    },
    tooltips: {
        callbacks: {
            title: function() {},
            beforeTitle: function(tooltipItem, data) {
                const index = tooltipItem[0].datasetIndex;
                return data.datasets[index].label;
            },
            label: function(tooltipItem) {
                const num = Number(tooltipItem.yLabel);
                let text = "лет";
                return num + " " + text;
            }
        },
        backgroundColor: "#fff",
        titleFontFamily: 'Lato',
        titleMarginBottom: 10,
        xPadding: 20,
        yPadding: 15,
        bodySpacing: 10,
        titleFontSize: 15,
        titleFontStyle: 'normal',
        titleFontColor: '#8181A5',
        bodyFontFamily: 'Lato',
        bodyFontColor: '#1C1D21',
        bodyFontSize: 15,
        bodyFontStyle: '700',
        shadowOffsetX: 0,
        shadowOffsetY: 8,
        shadowBlur: 8,
        shadowColor: 'rgba(152, 169, 188, 0.267182)',
        displayColors: false,
    }
};
const pieChartOption = {
    legend: {
        display: false
    },
    responsive: false,
    elements: {
        line:{
            tension: '0.000001'
        }
    },
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true,
                padding: 20,
                display: false
            },
            gridLines: {
                drawTicks: false,
                display: false
            }
        }],
        xAxes: [{
            gridLines: {
                zeroLineColor: "transparent",
                display: false
            },
            ticks: {
                padding: 0,
                fontColor: "rgba(0,0,0,0.5)",
                fontStyle: "bold",
                beginAtZero: true,
                display: false
            }
        }],
    },
    tooltips: {
        enabled: false
    }
};

// Add this function to graphs that are hidden on page after load.
function resizeChart(chart){  
    document.addEventListener('click', e => {
        setTimeout(() => {
            chart.resize();
        }, 10);
    });
}

const charts = document.querySelectorAll('.chart-js');

charts.forEach(chart => {
    const   option = chart.getAttribute('data-option'),
            ctxChart = chart.getContext('2d');

    if(option == 'multi'){
        //chart gradient
        const   gradientFill = ctxChart.createLinearGradient(0, 0, 0, 500);
                gradientFill.addColorStop(0, "rgba(77, 76, 172, 0.1)");
                gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.0001)");

        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'line',
            data: {
                labels: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек'],
                datasets: [
                    {
                        label: 'Проекты с критическими проблемами',
                        borderColor: '#FF808B',
                        pointBorderColor: '#FF808B',
                        pointBackgroundColor: '#FF808B',
                        pointHoverBackgroundColor: '#FF808B',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [8, 2, 5, 3, 5, 6, 8, 2, 5, 3, 5, 6],
                    },
                    {
                        label: 'Проекты с проблемами',
                        borderColor: '#FFCC00',
                        pointBorderColor: '#FFCC00',
                        pointBackgroundColor: '#FFCC00',
                        pointHoverBackgroundColor: '#FFCC00',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [10, 5, 7, 2, 6, 2, 5, 3, 2, 6, 2, 8],
                    },
                    {
                        label: 'Проекты в порядке',
                        borderColor: '#8181A5',
                        pointBorderColor: '#8181A5',
                        pointBackgroundColor: '#8181A5',
                        pointHoverBackgroundColor: '#8181A5',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [5, 2, 6, 3, 2, 4, 2, 7, 3, 3, 4, 1],
                    },
                ]
            },
            options: lineChartOptions
        });
    }else if(option == 'multi2'){
        const   gradientFill = ctxChart.createLinearGradient(0, 0, 0, 500);
                gradientFill.addColorStop(0, "rgba(77, 76, 172, 0.1)");
                gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.0001)");

        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'line',
            data: {
                labels: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек'],
                datasets: [
                    {
                        label: 'Здравоохранение',
                        borderColor: '#5E81F4',
                        pointBorderColor: '#5E81F4',
                        pointBackgroundColor: '#5E81F4',
                        pointHoverBackgroundColor: '#5E81F4',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [8, 2, 5, 3, 5, 6, 8, 2, 5, 3, 5, 6],
                    },
                    {
                        label: 'Культура',
                        borderColor: '#FFCC00',
                        pointBorderColor: '#FFCC00',
                        pointBackgroundColor: '#FFCC00',
                        pointHoverBackgroundColor: '#FFCC00',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [10, 5, 7, 2, 6, 2, 5, 3, 2, 6, 2, 8],
                    },
                    {
                        label: 'Автомобильные дороги',
                        borderColor: '#FF808B',
                        pointBorderColor: '#FF808B',
                        pointBackgroundColor: '#FF808B',
                        pointHoverBackgroundColor: '#FF808B',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [5, 2, 6, 3, 2, 4, 2, 7, 3, 3, 4, 1],
                    },
                    {
                        label: 'Человеческий капитал',
                        borderColor: '#7CE7AC',
                        pointBorderColor: '#7CE7AC',
                        pointBackgroundColor: '#7CE7AC',
                        pointHoverBackgroundColor: '#7CE7AC',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [0, 8, 4, 8, 4, 2, 9, 3, 7, 2, 7, 8],
                    },
                ]
            },
            options: lineChartOptions
        });
    }else if(option == 'multi-wg'){
        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'line',
            data: {
                labels: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек'],
                datasets: [
                    {
                        label: 'Здравоохранение',
                        borderColor: '#5E81F4',
                        pointBorderColor: '#5E81F4',
                        pointBackgroundColor: '#5E81F4',
                        pointHoverBackgroundColor: '#5E81F4',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [8, 2, 5, 3, 5, 6, 8, 2, 5, 3, 5, 6],
                    },
                    {
                        label: 'Культура',
                        borderColor: '#FFCC00',
                        pointBorderColor: '#FFCC00',
                        pointBackgroundColor: '#FFCC00',
                        pointHoverBackgroundColor: '#FFCC00',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [10, 5, 7, 2, 6, 2, 5, 3, 2, 6, 2, 8],
                    },
                    {
                        label: 'Автомобильные дороги',
                        borderColor: '#FF808B',
                        pointBorderColor: '#FF808B',
                        pointBackgroundColor: '#FF808B',
                        pointHoverBackgroundColor: '#FF808B',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [5, 2, 6, 3, 2, 4, 2, 7, 3, 3, 4, 1],
                    },
                    {
                        label: 'Человеческий капитал',
                        borderColor: '#7CE7AC',
                        pointBorderColor: '#7CE7AC',
                        pointBackgroundColor: '#7CE7AC',
                        pointHoverBackgroundColor: '#7CE7AC',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [0, 8, 4, 8, 4, 2, 9, 3, 7, 2, 7, 8],
                    },
                ]
            },
            options: lineDoubleChartOption
        });
        resizeChart(chartBox);
    }else if(option == 'multi-wg2'){
        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'line',
            data: {
                labels: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек'],
                datasets: [
                    {
                        label: 'Здравоохранение',
                        borderColor: '#5E81F4',
                        pointBorderColor: '#5E81F4',
                        pointBackgroundColor: '#5E81F4',
                        pointHoverBackgroundColor: '#5E81F4',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [8, 2, 5, 3, 5, 6, 8, 2, 5, 3, 5, 6],
                    },
                    {
                        label: 'Культура',
                        borderColor: '#FFCC00',
                        pointBorderColor: '#FFCC00',
                        pointBackgroundColor: '#FFCC00',
                        pointHoverBackgroundColor: '#FFCC00',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [10, 5, 7, 2, 6, 2, 5, 3, 2, 6, 2, 8],
                    },
                    {
                        label: 'Автомобильные дороги',
                        borderColor: '#FF808B',
                        pointBorderColor: '#FF808B',
                        pointBackgroundColor: '#FF808B',
                        pointHoverBackgroundColor: '#FF808B',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [5, 2, 6, 3, 2, 4, 2, 7, 3, 3, 4, 1],
                    },
                    {
                        label: 'Человеческий капитал',
                        borderColor: '#7CE7AC',
                        pointBorderColor: '#7CE7AC',
                        pointBackgroundColor: '#7CE7AC',
                        pointHoverBackgroundColor: '#7CE7AC',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        fill: true,
                        backgroundColor: 'rgba(255,255,255,0)',
                        borderWidth: 2,
                        data: [0, 8, 4, 8, 4, 2, 9, 3, 7, 2, 7, 8],
                    },
                ]
            },
            options: lineChartOptions
        });
        resizeChart(chartBox);
    }else if(option == 'bar'){
        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'bar',
            data: {
                labels: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек'],
                datasets: [
                    {
                        label: 'План',
                        borderColor: '#5E81F4',
                        pointBorderColor: '#5E81F4',
                        pointBackgroundColor: '#5E81F4',
                        pointHoverBackgroundColor: '#5E81F4',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: '#5E81F4',
                        borderWidth: 2,
                        data: [60, 65, 67, 80, 50, 45, 60, 35, 45, 23, 50, 55],
                        maxBarThickness: 12,
                    },
                    {
                        label: 'Факт',
                        borderColor: '#8AF1B9',
                        pointBorderColor: '#8AF1B9',
                        pointBackgroundColor: '#8AF1B9',
                        pointHoverBackgroundColor: '#8AF1B9',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: '#8AF1B9',
                        borderWidth: 2,
                        data: [50, 55, 57, 70, 40, 35, 50, 45, 35, 33, 60, 45],
                        maxBarThickness: 12,
                    },
                ]
            },
            options: barChartOption
        });

        resizeChart(chartBox);
    }else if(option == 'bar2'){
        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'bar',
            data: {
                labels: ['2022', '2023', '2024'],
                datasets: [
                    {
                        label: 'План',
                        borderColor: '#5E81F4',
                        pointBorderColor: '#5E81F4',
                        pointBackgroundColor: '#5E81F4',
                        pointHoverBackgroundColor: '#5E81F4',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: '#5E81F4',
                        borderWidth: 2,
                        data: [60, 65, 67],
                        maxBarThickness: 12,
                    },
                    {
                        label: 'Факт',
                        borderColor: '#8AF1B9',
                        pointBorderColor: '#8AF1B9',
                        pointBackgroundColor: '#8AF1B9',
                        pointHoverBackgroundColor: '#8AF1B9',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: '#8AF1B9',
                        borderWidth: 2,
                        data: [50, 55, 57],
                        maxBarThickness: 12,
                    },
                ]
            },
            options: barSmallChartOption
        });

        resizeChart(chartBox);
    }else if(option == 'bar-horiz'){
        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'horizontalBar',
            data: {
                labels: ['Формирование БП', 'Шаг 1', 'Шаг 1', 'Шаг 1'],
                datasets: [
                    {
                        label: 'План',
                        borderColor: ['#7CE7AC', '#7CE7AC', '#FFCC00', '#FF808B'],
                        pointBorderColor: ['#7CE7AC', '#7CE7AC', '#FFCC00', '#FF808B'],
                        pointBackgroundColor: ['#7CE7AC', '#7CE7AC', '#FFCC00', '#FF808B'],
                        pointHoverBackgroundColor: ['#7CE7AC', '#7CE7AC', '#FFCC00', '#FF808B'],
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: true,
                        backgroundColor: ['#7CE7AC', '#7CE7AC', '#FFCC00', '#FF808B'],
                        borderWidth: 2,
                        data: [60, 40, 30, 20],
                        maxBarThickness: 24,
                    },
                ]
            },
            options: barHorzChartOption
        });

        resizeChart(chartBox);
    }else if(option == 'double'){
        const   gradientFill = ctxChart.createLinearGradient(0, 0, 0, 500);
                gradientFill.addColorStop(0, "rgba(77, 76, 172, 0.1)");
                gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.0001)");

        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'line',
            data: {
                labels: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек'],
                datasets: [
                    {
                        label: 'План',
                        borderColor: '#5E81F4',
                        pointBorderColor: '#5E81F4',
                        pointBackgroundColor: '#5E81F4',
                        pointHoverBackgroundColor: '#5E81F4',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: 'start',
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [-4, -10, -5, -3, -5, -6, -8, -2, -5, -3, -5, -6],
                    },
                    {
                        label: 'Факт',
                        borderColor: '#8AF1B9',
                        pointBorderColor: '#8AF1B9',
                        pointBackgroundColor: '#8AF1B9',
                        pointHoverBackgroundColor: '#8AF1B9',
                        pointHoverBorderColor: 'rgba(94, 129, 244, 0.1)',
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 1,
                        pointRadius: 3,
                        fill: 'start',
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [-10, -5, -7, -2, -6, -2, -5, -3, -2, -6, -2, -8],
                    },
                ]
            },
            options: lineDoubleChartOption
        });
    
        resizeChart(chartBox);
    }else if(option ==  'pie'){

        const chartBox = new Chart(ctxChart, {
            responsive: false,
            type: 'doughnut',
            data: {
                labels: ['Человеческий капитал', 'Экономический рост', 'Комфортная среда для жизни', 'Инфраструктура'],
                datasets: [
                    {   
                        
                        data: [5.7, 10.1, 9.9, 9.9],
                        backgroundColor: ['#5E81F4', '#309282', '#FF9900', '#484873'],
                        hoverBackgroundColor: ['#2f5cf1', '#006352', '#c77800', '#2b2b52'],
                        hoverBorderColor: '#fff',
                    },
                    {
                        labels: ['a', 'b', 'c', 'd'],
                        data: [13158.2, 13158.2, 13158.2, 13158.2],
                        backgroundColor: ['#7CE7AC', '#FF808B', '#FFCC00', '#979797'],
                        hoverBackgroundColor: ['#5ca57c', '#bb5a63', '#9e8006', '#4e4d4d'],
                        borderWidth: 20,
                        hoverBorderColor: '#fff',
                    },
                ]
            },
            options: pieChartOption
        });

        document.querySelector('.js-hover-1').addEventListener('mouseenter', () => {t(0,0);});
        document.querySelector('.js-hover-2').addEventListener('mouseenter', () => {t(0,1);});
        document.querySelector('.js-hover-3').addEventListener('mouseenter', () => {t(0,2);});
        document.querySelector('.js-hover-4').addEventListener('mouseenter', () => {t(0,3);});
        document.querySelector('.js-hover-5').addEventListener('mouseenter', () => {t(1,0);});
        document.querySelector('.js-hover-6').addEventListener('mouseenter', () => {t(1,1);});
        document.querySelector('.js-hover-7').addEventListener('mouseenter', () => {t(1,2);});
        document.querySelector('.js-hover-8').addEventListener('mouseenter', () => {t(1,3);});

        function t(dataset, idx) {
            let meta = chartBox.getDatasetMeta(dataset),
                rect = chartBox.canvas.getBoundingClientRect(),
                point = meta.data[idx].getCenterPoint(),
                evt = new MouseEvent('mousemove', {
                    clientX: rect.left + point.x,
                    clientY: rect.top + point.y
                }),
                node = chartBox.canvas;

            node.dispatchEvent(evt);
        }
    
        resizeChart(chartBox);
    }
});