const   tabs = document.querySelectorAll('[data-tab]'),
        tabsBlocks = document.querySelectorAll('[data-tab-block]');

function hideTabs(item){    
    const group = item.getAttribute('data-tab-group'); 

    if(group && group != ''){
        const newTabs = [...tabs].filter((tab) => {
            return tab.getAttribute('data-tab-group') == group;
        });
        newTabs.forEach(tab => {
            tab.classList.remove('active');
        });

        const newBlocks = [...tabsBlocks].filter((tab) => {
            return tab.getAttribute('data-tab-group') == group;
        });

        newBlocks.forEach(block => {
            block.classList.add('hide');
        });
    }else{
        tabsBlocks.forEach(block => {
            block.classList.add('hide');
        });
        
        tabs.forEach(tab => {
            tab.classList.remove('active');
        });
    }
}

function showTabs(item){
    const   data = item.getAttribute('data-tab'),
            group = item.getAttribute('data-tab-group'); 

    if(group && group != ''){
        document.querySelector(`[data-tab-group=${group}][data-tab=${data}]`).classList.add('active');
        document.querySelector(`[data-tab-group=${group}][data-tab-block=${data}]`).classList.remove('hide');
    }else{
        document.querySelector(`[data-tab=${data}]`).classList.add('active');
        document.querySelector(`[data-tab-block=${data}]`).classList.remove('hide');
    }
}

tabs.forEach(tab => {
    tab.addEventListener('click', e => {
        e.preventDefault();
        
        hideTabs(tab);
        showTabs(tab);
    });
});