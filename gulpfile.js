'use strict';

const gulp = require('gulp'),
	prefix = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify-es').default,
	sass = require('gulp-sass'),
	rigger = require('gulp-rigger'),
	rimraf = require('rimraf');

const path = {
	build: { //Тут мы укажем куда складывать готовые после сборки файлы
		html: 'dist/',
		js: 'dist/js/',
		css: 'dist/css/',
		img: 'dist/img/',
		fonts: 'dist/fonts/'
	},
	src: { //Пути откуда брать исходники
		html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
		js: 'src/js/*.*',//В стилях и скриптах нам понадобятся только main файлы
		style: 'src/scss/*.*',
		img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
		fonts: 'src/fonts/**/*.*'
	},
	watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		style: 'src/scss/**/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*'
	},
	clean: './dist'
};

/*********************************
		Developer tasks
*********************************/

// js
gulp.task('js:build', function () {
    return gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(uglify()) //Сожмем наш js
        .pipe(gulp.dest(path.build.js)); //Выплюнем готовый файл в build
});

// style
gulp.task('style:build', function () {
    return gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError)) //Скомпилируем
        .pipe(prefix()) //Добавим вендорные префиксы
        .pipe(gulp.dest(path.build.css)); //И в build
});

// image
gulp.task('image:build', function () {
    return gulp.src(path.src.img) //Выберем наши картинки
        .pipe(gulp.dest(path.build.img));
});

// fonts
gulp.task('fonts:build', function() {
    return gulp.src('src/fonts/**/*.*')
        .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('build', gulp.parallel(
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
));

gulp.task('watch', function(){
	gulp.watch(path.watch.style, gulp.series('style:build'));
	gulp.watch(path.watch.js, gulp.series('js:build'));
	gulp.watch(path.watch.img, gulp.series('image:build'));
	gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', gulp.parallel('build', 'watch'));